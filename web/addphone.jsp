<%-- 
    Document   : addphone
    Created on : Dec 10, 2018, 9:01:50 AM
    Author     : viett
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>AddPhone Page</title>
    </head>
    <body>
        <table align="center" id="tb2">
            <form action="addPhone" method="post">
                <tr>
                    <td>Name:</td>
                    <td><input type="text" name="phone_name" required></td>
                </tr>
                <tr>
                    <td>Brand:</td>
                    <td>
                        <select type="text" name="phone_brand">
                            <option value="Nokia">Nokia</option>
                            <option value="SamSung">SamSung</option>
                            <option value="Apple">Apple</option>
                            <option value="Others">Others</option>
                        </select>
                    </td>
                </tr>
                <tr>
                    <td>Price</td>
                    <td><input type="text" name="phone_price" required></td>
                </tr>
                <tr>
                    <td>Description:</td>
                    <td><input type="text" name="phone_des" required></td>
                </tr>
                <tr>
                    <td><input type="submit" value="Add"></td>
                    <td><input type="reset" value="Reset"></td>
                </tr>
            </form>
        </table>
    </body>
</html>
